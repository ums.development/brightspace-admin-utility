# Brightspace Administrator Utility

[Software Artifact Page]{https://gojira.its.maine.edu/confluence/display/WB/BrightSpace+Administrative+Utility

User facing utility application to allow manual operations outside of what is handled through the automatic integration processes.

## Development

### Items you'll need for local development

- [Go](https://golang.org/doc/install).
- [Node.js](https://nodejs.org/en/).
- [yarn](https://yarnpkg.com/getting-started/install).

#### Linux
- [Oracle Instant Client](https://www.oracle.com/database/technologies/instant-client/downloads.html).
- [Oracle Go Driver](https://github.com/mattn/go-oci8#installation).

#### Windows
- Install [Oracle Basic Instant Client and SDK](https://www.oracle.com/database/technologies/instant-client/downloads.html).
- Add oracle instant client directory to PATH
- Install [MinGW](https://www.mingw-w64.org/) 
- Add MinGW bin directory to PATH
- Install [pkg-config](https://gojira.its.maine.edu/confluence/pages/viewpage.action?spaceKey=~yan.liu&title=Oracle+go+driver+in+Windows10) 
- Create oci8.pc file as follows substituting your Instant Client directory and set PKG_CONFIG_PATH to the directory containing it.

```
libdir=<Oracle Instant Client Home>/sdk/lib/msvc
includedir=<Oracle Instant Client Home>/sdk/include

glib_genmarshal=glib-genmarshal
gobject_query=gobject-query
glib_mkenums=glib-mkenums

Name: oci8
Description: oci8 library
Libs: -L${libdir} -loci
Cflags: -I${includedir}
Version: 12.2
```

- Install [Oracle Go Driver](https://github.com/mattn/go-oci8#installation).

```
go get github.com/mattn/go-oci8
```

### For Continuous Building

#### Server

- Install [Task](https://taskfile.dev/).
- run: `task -w`.
- Note: this will also lint the server code upon a change.

##### run Server through docker

- If you choose to run server through docker, you don't need to install Oracle Instant Clien,
  Oracle Go Driver and Task on your local machcin.
- You need run: `docker-compose up` and the server should be up
- Note: this will also lint the server code upon a change.

#### Client
Set CLIENT_BASE to local server URL in .env.  
Client can be run using `cd client && yarn dev`.
Note: When not in production mode, the webpack analyzer will be available on port [8888](http://localhost:8888).

#### Logging Debug messages

Set the environment variable, ENVIRONMENT, to anything except `production`.

### Authentication

To bypass authentication, set AUTH_TYPE=NOAUTH in [env](/.env). Setting SPOOFED_ID=[username], will set the authenticated user.

### Dockerfile linting

Run `task docker-lint`.

### Docker Environment

If you would prefer to use the docker environment locally, you will need to include the following environment variables with the run command.

- SPLUNK_HOST.
- SPLUNK_TOKEN.
- SPLUNK_INDEX.

## Environment Variables

| Variable            | Description                                                                                                                                                       | Value                                                                                                                                                       |
| ------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| DB\*\*              | Database credentials                                                                                                                                              | N/A                                                                                                                                                         |
| LMS_REST_API        | Hostname of the [Brightspace REST API](https://gitlab.com/ums.development/brightspace-rest-api)                                                                   | http://localhost.localdomain:5000                                                                                                                           |
| PORT                | Server port to listen on                                                                                                                                          | 5000                                                                                                                                                        |
| HTTP_CLIENT_TIMEOUT | Timeout in milliseconds                                                                                                                                           | 4000                                                                                                                                                        |
| LMS_TOKEN           | [JSON Web Token](https://jwt.io/) to connect to the REST API. Use `yarn token` in [Brightspace REST API](https://gitlab.com/ums.development/brightspace-rest-api) | eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c |
| API_URL             | URL/Prefix to use on the server for the REST API routes                                                                                                           | /rest/                                                                                                                                                      |
| AUTH_TYPE           | Authentication type: [CAS, NOAUTH]                                                                                                                                | CAS                                                                                                                                                         |
| AUTH_CONNECTION     | Authentication connection                                                                                                                                         | https://identity.maine.edu/cas/                                                                                                                             |
| SPOOFED_ID          | Used to set the authenticated user, see [NOAUTH][authentication]                                                                                                  | calvinb                                                                                                                                                     |
| ENVIRONMENT         | Used to change the logging behavior                                                                                                                               | production                                                                                                                                                  |
| SPLUNK_HOST         | Splunk host to connect to                                                                                                                                         | splunk-ces-dev.its.maine.edu                                                                                                                                |
| SPLUNK_TOKEN        | Splunk HEC Token                                                                                                                                                  | 19101b15-32fb-4d48-a4db-9df530887b9c                                                                                                                        |
| SPLUNK_INDEX        | Splunk index to use                                                                                                                                               | brightspace                                                                                                                                                 |
