export default function ({ store, route, redirect, $axios }) {
	if (store.getters["Navbar/username"] === "") {
		get_authedUser(store, $axios).then(function () {
			if (!checkAuthorized(store, route.name)) {
				return redirect("/unauthorized");
			}
		});
	} else {
		if (!checkAuthorized(store, route.name)) {
			return redirect("/unauthorized");
		}
	}
}

function get_authedUser(store, axios) {
	return new Promise(function (resolve) {
		axios
			.$get("/system/authedUser/")
			.then(function (res) {
				store.commit("Navbar/set_username", res.username);
				store.commit("Navbar/set_showAdminHelpInfo", res.showAdminHelpInfo);
				store.commit("Navbar/set_permissions", res.permissions);
				store.commit("Navbar/set_pages", res.pages);
			})
			.catch(function () {
				store.commit("Navbar/set_username", "");
			})
			.finally(function () {
				resolve();
			});
	});
}

function checkAuthorized(store, routeToName) {
	if (routeToName === "unauthorized") {
		return true;
	}

	return store.getters["Navbar/hasPage"](routeToName);
}
