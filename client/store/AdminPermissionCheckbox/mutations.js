export default {
	set_adminPermissionCheckboxValue(state, payload) {
		let found = false;
		for (let i = 0; i < state.adminPermissionCheckboxValues.length; i++) {
			if (state.adminPermissionCheckboxValues[i].name === payload.name) {
				found = true;
				state.adminPermissionCheckboxValues[i].value = payload.value;
				break;
			}
		}

		if (!found) {
			state.adminPermissionCheckboxValues.push(payload);
		}
	},

	clear_adminPermissionCheckboxValues(state) {
		state.adminPermissionCheckboxValues = [];
	},
};
