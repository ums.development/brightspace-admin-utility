export default {
	alerts(state) {
		return state.alerts;
	},

	is_alert(state) {
		return state.alerts.length > 0;
	},
};
