export default {
	set_alerts(state, payload) {
		state.alerts = payload;
	},

	clear_alerts(state) {
		state.alerts = [];
	},
};
