export default {
	get_course_settings(aaa, campus) {
		let vThis = this;
		return new Promise(function (resolve, reject) {
			vThis.$axios
				.$get("/courseSettings/" + campus + "/")
				.then(function (res) {
					resolve(res);
				})
				.catch(function (err) {
					dispatch(
						"Alerts/add_alert",
						{
							variant: "warning",
							msg:
								"Unable to retrieve" +
								campus +
								" course settings with error: " +
								err,
						},
						{ root: true }
					);
					reject(err);
				});
		});
	},

	update_course_settings({ dispatch }, payload) {
		payload.startDateOffset = parseInt(payload.startDateOffset);
		payload.endDateOffset = parseInt(payload.endDateOffset);

		let vThis = this;
		return new Promise(function (resolve, reject) {
			vThis.$axios
				.$put("/courseSettings/" + payload.campus + "/", payload)
				.then(function () {
					resolve();
				})
				.catch(function (resError) {
					dispatch(
						"Alerts/add_alert",
						{
							variant: "warning",
							msg: "Unable to save your change: " + resError,
						},
						{ root: true }
					);
					reject();
				});
		});
	},
};
