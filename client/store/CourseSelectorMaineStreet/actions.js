export default {
	get_coursesMS({ commit }, payload) {
		commit("set_coursesMS", []);
		commit("set_courses_fetching", true);

		let queryArr = [];
		pushQuery("campus", payload, queryArr);
		pushQuery("term", payload, queryArr);
		pushQuery("subject", payload, queryArr);
		pushQuery("number", payload, queryArr);

		let query = queryArr.join("&");
		let url = payload.data_source;
		if (query.length > 0) {
			url = url + "?" + query;
		}

		this.$axios
			.$get(url)
			.then(function (res) {
				commit("set_coursesMS", res);
				commit("set_courses_fetching", false);
			})
			.catch(function () {
				commit("set_coursesMS", []);
				commit("set_courses_fetching", false);
			});
	},

	get_course_lms_id(store, courseCode) {
		let vThis = this;
		return new Promise(function (resolve, reject) {
			vThis.$axios
				.$get("/lmsCourse/byCode/?code=" + courseCode)
				.then(function (res) {
					resolve(res.Identifier);
				})
				.catch(function (err) {
					reject(err);
				});
		});
	},
};

function pushQuery(property, source, destination) {
	if (source[property] && source[property].length > 0) {
		destination.push(property + "=" + source[property]);
	}
}
