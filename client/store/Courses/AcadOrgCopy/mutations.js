export default {
	set_fetching(state, payload) {
		state.fetching = payload;
	},

	set_courses(state, payload) {
		state.courses = payload;
	},
};
