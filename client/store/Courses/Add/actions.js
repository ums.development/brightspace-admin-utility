export default {
	get_bs_course_lms_id({ commit }, courseCode) {
		commit("set_bs_course_lms_id_fetching", true);
		this.$axios
			.$get("/lmsCourse/byCode/?code=" + courseCode)
			.then(function (res) {
				commit("set_bs_course_lms_id", res.Identifier);
				commit("set_bs_course_lms_id_fetching", false);
			})
			.catch(function () {
				commit("set_bs_course_lms_id", null);
				commit("set_bs_course_lms_id_fetching", false);
			});
	},
	create_course({ commit, dispatch }, courseCode) {
		commit("set_bs_course_lms_id_fetching", true);
		this.$axios
			.$post("/lmsCourse/", { code: courseCode })
			.then(function (res) {
				commit("set_bs_course_lms_id", res.Identifier);
				commit("set_bs_course_lms_id_fetching", false);
			})
			.catch(function () {
				commit("set_bs_course_lms_id", null);
				commit("set_bs_course_lms_id_fetching", false);
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to create the course, " + courseCode,
					},
					{ root: true }
				);
			});
	},
};
