export default {
	bs_delete_course_lms_id(state) {
		return state.bs_delete_course_lms_id;
	},

	bs_delete_course_lms_id_fetching(state) {
		return state.bs_delete_course_lms_id_fetching;
	},
	bs_recreate_course_lms_id(state) {
		return state.bs_recreate_course_lms_id;
	},

	bs_recreate_course_lms_id_fetching(state) {
		return state.bs_recreate_course_lms_id_fetching;
	},
};
