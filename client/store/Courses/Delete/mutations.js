export default {
	set_bs_delete_course_lms_id(state, payload) {
		state.bs_delete_course_lms_id = payload;
	},

	set_bs_delete_course_lms_id_fetching(state, payload) {
		state.bs_delete_course_lms_id_fetching = payload;
	},

	set_bs_recreate_course_lms_id(state, payload) {
		state.bs_recreate_course_lms_id = payload;
	},

	set_bs_recreate_course_lms_id_fetching(state, payload) {
		state.bs_recreate_course_lms_id_fetching = payload;
	},
};
