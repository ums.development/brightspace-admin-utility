export default {
	get_ms_enrollments({ commit }, courseCode) {
		commit("set_ms_enrollments_fetching", true);
		this.$axios
			.$get("/enrollments/byCode/?code=" + courseCode)
			.then(function (res) {
				commit("set_ms_enrollments", res);
				commit("set_ms_enrollments_fetching", false);
			})
			.catch(function () {
				commit("set_ms_enrollments", []);
				commit("set_ms_enrollments_fetching", false);
			});
	},

	get_bs_enrollments({ commit }, courseCode) {
		commit("set_bs_enrollments_fetching", true);
		commit("set_changed_enrollments", []);
		this.$axios
			.$get("/lmsEnrollments/byCode/?code=" + courseCode)
			.then(function (res) {
				commit("set_bs_enrollments", res);
				commit("set_bs_enrollments_fetching", false);
			})
			.catch(function () {
				commit("set_bs_enrollments", []);
				commit("set_bs_enrollments_fetching", false);
			});
	},

	enrollment_action({ commit, getters }, payload) {
		let method = "post";
		switch (payload.action) {
			case "Delete":
			case "Drop":
				method = "delete";
				break;
		}
		let data = {
			courseCode: payload.course_code,
			emplid: payload.emplid,
			courseLmsId: payload.course_lms_id,
			userLmsId: payload.user_lms_id,
		};

		this.$axios({
			method: method,
			url: "/lmsEnrollment/",
			data: data,
		})
			.then(function (res) {
				let enrollData = payload;
				enrollData.status = "Success";
				commit("push_changed_enrollments", enrollData);

				let bs_enrolls = getters.bs_enrollments;
				if (method === "delete") {
					bs_enrolls.splice(
						bs_enrolls.findIndex((e) => e.OrgDefinedId === payload.emplid),
						1
					);
				} else {
					let bsRoleName = "";
					switch (res.type) {
						case "I":
							bsRoleName = "Instructor";
							break;
						case "S":
							bsRoleName = "Learner";
							break;
						default:
							bsRoleName = "Other";
					}

					let foundI = bs_enrolls.findIndex(
						(e) => e.OrgDefinedId === enrollData.emplid
					);
					if (foundI !== -1) {
						bs_enrolls[foundI].RoleName = bsRoleName;
					} else {
						let newEnrollment = {
							OrgDefinedId: enrollData.emplid,
							RoleName: bsRoleName,
						};
						bs_enrolls.push(newEnrollment);
					}
				}

				commit("set_bs_enrollments", bs_enrolls);
			})
			.catch(function () {
				let enrollData = payload;
				enrollData.status = "Failed";

				commit("push_changed_enrollments", enrollData);
			});
	},
};
