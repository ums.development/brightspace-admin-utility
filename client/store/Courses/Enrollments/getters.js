export default {
	ms_enrollments(state) {
		return state.ms_enrollments;
	},

	ms_enrollments_fetching(state) {
		return state.ms_enrollments_fetching;
	},

	bs_enrollments(state) {
		return state.bs_enrollments;
	},

	bs_enrollments_fetching(state) {
		return state.bs_enrollments_fetching;
	},

	changed_enrollments(state) {
		return state.changed_enrollments;
	},
};
