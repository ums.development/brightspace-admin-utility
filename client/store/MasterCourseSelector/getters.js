export default {
	universities(state) {
		return state.universities;
	},

	colleges(state) {
		return state.colleges;
	},

	academic_units(state) {
		return state.academic_units;
	},
};
