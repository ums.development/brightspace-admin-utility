export default {
	colleges_all(state) {
		return state.colleges_all;
	},

	academic_units_all(state) {
		return state.academic_units_all;
	},
};
