export default {
	get_environment({ commit }) {
		this.$axios
			.$get("/system/environment/")
			.then(function (res) {
				commit("set_environment", res.environment);
			})
			.catch(function (error) {
				commit("set_environment", "");
			});
	},
};
