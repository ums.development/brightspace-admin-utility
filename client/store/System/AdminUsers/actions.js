export default {
	get_adminUsers({ commit }) {
		commit("set_adminUsersFetching", true);
		this.$axios
			.$get("/adminUser/all/")
			.then(function (res) {
				commit("set_adminUsers", res);
				commit("set_adminUsersFetching", false);
			})
			.catch(function () {
				commit("set_adminUsers", []);
			});
	},

	delete_adminUser({ dispatch }, payload) {
		this.$axios
			.$delete("/adminUser/" + payload.id + "/")
			.then(function () {
				dispatch("get_adminUsers");
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
			});
	},

	add_adminUser({ dispatch }, payload) {
		this.$axios
			.$post("/adminUser/", payload)
			.then(function () {
				dispatch("get_adminUsers");
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
			});
	},

	edit_adminUser({ dispatch }, payload) {
		let adminUser = {
			username: payload.username,
			showAdminHelpInfo: payload.showAdminHelpInfo,
			isActive: payload.isActive,
		};
		this.$axios
			.$put("/adminUser/" + payload.id + "/", adminUser)
			.then(function () {
				dispatch("get_adminUsers");
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to save your change: " + resError,
					},
					{ root: true }
				);
			});
	},
};
