export default {
	termsToProcess(state) {
		return state.termsToProcess;
	},

	coursesStats(state) {
		return state.coursesStats;
	},

	termsFetching(state) {
		return state.termsFetching;
	},

	statsFetching(state) {
		return state.statsFetching;
	},
};
