export default {
	set_users_ms(state, payload) {
		state.users_ms = payload;
	},

	set_users_ms_fetching(state, payload) {
		state.users_ms_fetching = payload;
	},

	set_user_search_id(state, payload) {
		state.user_search_id = payload;
	},
};
