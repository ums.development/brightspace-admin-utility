export default {
	get_user_enrollments_system({ commit }, emplid) {
		commit("set_user_enrollments_system_fetching", true);
		this.$axios
			.$get("/enrollments/system/?emplid=" + emplid)
			.then(function (res) {
				commit("set_user_enrollments_system", res);
				commit("set_user_enrollments_system_fetching", false);
			})
			.catch(function () {
				commit("set_user_enrollments_system", null);
				commit("set_user_enrollments_system_fetching", false);
			});
	},

	update_user_enrollments_system({ commit, dispatch }, payload) {
		commit("set_user_enrollments_system_fetching", true);
		this.$axios
			.$put("/enrollments/system/", {
				emplid: payload.emplid,
				role: payload.role,
			})
			.then(function () {
				dispatch("get_user_enrollments_system", payload.emplid);
			})
			.catch(function () {
				commit("set_user_enrollments_system", null);
				commit("set_user_enrollments_system_fetching", false);
			});
	},
};
