export default {
	get_role_overrides({ commit }) {
		commit("set_fetching", true);
		commit("set_role_overrides", []);

		this.$axios
			.$get("/roleOverride/")
			.then(function (res) {
				commit("set_role_overrides", res);
				commit("set_fetching", false);
			})
			.catch(function () {
				commit("set_role_overrides", []);
				commit("set_fetching", false);
			});
	},
	get_all_roles({ commit }) {
		commit("set_roles", []);

		this.$axios
			.$get("/roleOverride/all-roles/")
			.then(function (res) {
				commit("set_roles", res);
			})
			.catch(function () {
				commit("set_roles", []);
			});
	},
	delete_role_override({ commit, dispatch }, payload) {
		this.$axios
			.$delete("/roleOverride/" + payload.userLmsId + "/")
			.then(function () {
				dispatch("get_role_overrides");
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to remove the user role override: " + resError,
					},
					{ root: true }
				);
			});
	},
	add_role_override({ commit, dispatch }, payload) {
		this.$axios
			.$post("/roleOverride/", payload)
			.then(function () {
				dispatch("get_role_overrides");
			})
			.catch(function (resError) {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to add/edit the user role override: " + resError,
					},
					{ root: true }
				);
			});
	},
};
