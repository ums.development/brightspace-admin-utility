export default {
	role_overrides(state) {
		return state.role_overrides;
	},

	fetching(state) {
		return state.fetching;
	},

	roles(state) {
		return state.roles;
	},
};
