export default {
	get_ms_user_info({ commit }, emplid) {
		commit("set_ms_user_info_fetching", true);
		commit("set_ms_user_info", []);

		this.$axios
			.$get("/users/?userId=" + emplid)
			.then(function (res) {
				commit("set_ms_user_info", res[0]);
				commit("set_ms_user_info_fetching", false);
			})
			.catch(function () {
				commit("set_ms_user_info", []);
				commit("set_ms_user_info_fetching", false);
			});
	},

	get_bs_user_info({ commit }, emplid) {
		commit("set_bs_user_info_fetching", true);
		commit("set_bs_user_info", []);
		this.$axios
			.$get("/lmsUser/byEmplid/?emplid=" + emplid)
			.then(function (res) {
				commit("set_bs_user_info", res);
				commit("set_bs_user_info_fetching", false);
			})
			.catch(function () {
				commit("set_bs_user_info", []);
				commit("set_bs_user_info_fetching", false);
			});
	},

	update_bs_user_info({ commit, dispatch }, payload) {
		commit("set_bs_user_info_fetching", true);
		this.$axios
			.$put("/lmsUser/byEmplid/?emplid=" + payload.emplid, {
				FirstName: payload.firstName,
				LastName: payload.lastName,
				MiddleName: payload.middleName,
				ExternalEmail: payload.email,
				OrgDefinedId: payload.emplid,
				UserName: payload.username,
			})
			.then(function () {
				dispatch("get_bs_user_info", payload.emplid);
			})
			.catch(function () {
				dispatch(
					"Alerts/add_alert",
					{
						variant: "warning",
						msg: "Unable to refresh user information for " + payload.username,
					},
					{ root: true }
				);
				commit("set_bs_user_info_fetching", false);
			});
	},
};
