export default {
	ms_user_info(state) {
		return state.ms_user_info;
	},

	ms_user_info_fetching(state) {
		return state.ms_user_info_fetching;
	},

	bs_user_info(state) {
		return state.bs_user_info;
	},

	bs_user_info_fetching(state) {
		return state.bs_user_info_fetching;
	},
};
