package handlers

import (
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"io/ioutil"
	"net/http/httptest"
	"testing"
)

func TestIndex(t *testing.T) {
	Describe("Index page", func() {
		req := httptest.NewRequest("GET", "/", nil)

		rec := httptest.NewRecorder()
		Index(rec, req)

		res := rec.Result()
		defer res.Body.Close()

		It("Should get a status of 200", func() {
			Expect(res.StatusCode).To(Equal(200))
		})

		b, _ := ioutil.ReadAll(res.Body)
		s := string(b)

		It("Body should have a title", func() {
			Expect(s).To(ContainSubstring("<div id=\"app\">"))
		})
	})
}
