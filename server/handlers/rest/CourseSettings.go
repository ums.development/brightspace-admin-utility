package rest

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
)

func CourseSettingsUpdate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	campus := vars["campus"]

	err := db.ValidateInstitution(campus)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	cs := db.CourseSettings{}
	err = json.NewDecoder(r.Body).Decode(&cs)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	cs.Institution = campus
	if cs.SetStartDate == false {
		cs.StartDateDaysOffset = 0
	}
	if cs.SetEndDate == false {
		cs.EndDateDaysOffset = 0
	}

	err = db.UpdateCourseSettings(cs)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func CourseSettingsGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	campus := vars["campus"]

	err := db.ValidateInstitution(campus)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	cs, err := db.GetCourseSettings(campus)

	_ = json.NewEncoder(w).Encode(cs)
}
