package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/rest"
	"net/http"
)

func AcademicUnitGetSet(w http.ResponseWriter, r *http.Request) {
	items, err := getAcademicUnits(false)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	if items != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(items)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func AcademicUnitGetAll(w http.ResponseWriter, r *http.Request) {
	items, err := getAcademicUnits(true)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	if items != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(items)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func getAcademicUnits(includeItemsWithNoTemplateCourse bool) (items []db.AcademicUnit, err error) {
	dbConn := helpers.DbConn()
	query := "Select CODE, NAME, PARENT, LMS_ID, nvl(TEMPLATE_LMS_ID_FALL, '<null>') TEMPLATE_LMS_ID_FALL, nvl(TEMPLATE_LMS_ID_SPRING, '<null>') TEMPLATE_LMS_ID_SPRING, nvl(TEMPLATE_LMS_ID_SUMMER, '<null>') TEMPLATE_LMS_ID_SUMMER From UNIT_COMPLETED"
	if !includeItemsWithNoTemplateCourse {
		query += " Where TEMPLATE_LMS_ID_FALL Is Not Null Or TEMPLATE_LMS_ID_SPRING Is Not Null Or TEMPLATE_LMS_ID_SUMMER Is Not Null"
	}
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Select(&items, query)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			items = nil
			return
		}

		err = fmt.Errorf("query, <<<%s>>>, failed with error: %+v", query, err)
	}

	for i := range items {
		if items[i].TemplateLmsIdFall == "<null>" {
			items[i].TemplateLmsIdFall = ""
		}
		if items[i].TemplateLmsIdSpring == "<null>" {
			items[i].TemplateLmsIdSpring = ""
		}
		if items[i].TemplateLmsIdSummer == "<null>" {
			items[i].TemplateLmsIdSummer = ""
		}
	}

	return
}

func AcademicUnitUpdate(w http.ResponseWriter, r *http.Request) {
	var academicUnit rest.AcademicUnit
	err := json.NewDecoder(r.Body).Decode(&academicUnit)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	vars := mux.Vars(r)
	academicUnit.LmsId = vars["id"]

	termTemplate := "TEMPLATE_LMS_ID_" + academicUnit.Term

	query := fmt.Sprintf("Update UNIT_COMPLETED Set %s = :TEMPLATE_LMS_ID Where LMS_ID = :LMS_ID", termTemplate)
	if len(academicUnit.TemplateLmsId) == 0 {
		query = fmt.Sprintf("Update UNIT_COMPLETED Set %s = NULL Where LMS_ID = :LMS_ID", termTemplate)
	}
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s, %s", query, &academicUnit))
	sqlResult, err := helpers.DbConn().NamedExec(query, &academicUnit)
	helpers.Logger().Debugw(fmt.Sprintf("Complete query %s, %s", query, &academicUnit))
	if err != nil {
		helpers.HttpWriteError(w, r, fmt.Errorf("error updating academic unit, %s, with error: %+v", academicUnit.LmsId, err))
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		helpers.HttpRespond500(w, r, fmt.Errorf("error updating academic unit, %s, with error: %+v", academicUnit.LmsId, err))
		return
	}
	if rowsAffected != 1 {
		helpers.HttpRespond500(w, r, fmt.Errorf("error updating academic unit, %s", academicUnit.LmsId))
		return
	}

	w.WriteHeader(http.StatusOK)
}
