package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
	"regexp"
)

func GetCourseByCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	err := ValidateCourseCode(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	course, err := db.GetCourseByCode(code)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(course)
}

func ValidateCourseCode(code string) error {
	re := regexp.MustCompile(`^\d{4}\.UMS\d{2}-[C|S]\.\d+\.?\w*$`)
	if !re.MatchString(code) {
		return fmt.Errorf("invalid code, %s", code)
	}

	return nil
}
