package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
)

func CoursesStats(w http.ResponseWriter, r *http.Request) {
	stats, err := getCourseStats()
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	if stats != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(stats[0])
	} else {
		w.WriteHeader(http.StatusNotFound)
	}

}

func getCourseStats() (stats []db.CourseStats, err error) {
	dbConn := helpers.DbConn()
	query := "Select QUEUED_COURSES, PROCESS_QUANTITY From LMS_COURSES_PROCESSING_STATS"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Select(&stats, query)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			stats = nil
			return
		}
		err = fmt.Errorf("query, <<<%s>>>, failed: %+v", query, err)
	}

	return
}
