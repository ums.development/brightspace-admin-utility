package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
	"gitlab.its.maine.edu/development/lms/utility/server/models/rest"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func CoursesByAcadOrg(w http.ResponseWriter, r *http.Request) {
	cao := courseAcadOrg{
		Institution: getQueryParam(r, "campus"),
		Term:        getQueryParam(r, "term"),
		AcadOrg:     getQueryParam(r, "acadOrg"),
	}

	errs := validateCourseAcadOrg(cao)
	if len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}

	courses, err := getCoursesByAcadOrg(cao)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
	} else if courses != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(courses)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func CoursesBulkCopy(w http.ResponseWriter, r *http.Request) {
	var cbc rest.CourseBulkCopy
	err := json.NewDecoder(r.Body).Decode(&cbc)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	errs := rest.CourseBulkCopyValidate(cbc)
	if len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}

	var errors []error
	for _, course := range cbc.CoursesToCopy {
		dbCourse, err := db.GetCourseById(course.LmsId)
		if err != nil {
			errors = append(errors, err)
			continue
		}

		masterCourseCode, err := getMasterCourseCodeForCopy(cbc.Institution, cbc.AcadOrg, dbCourse.Subject, dbCourse.CatalogNumber)

		if err != nil {
			errors = append(errors, err)
			continue
		}

		err = lms.CourseCopy(course.LmsId, masterCourseCode)
		if err != nil {
			errors = append(errors, err)
		}
	}

	if len(errors) > 0 {
		helpers.HttpRespond500(w, r, errors)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}

func getMasterCourseCodeForCopy(institution string, academicOrganization string, subject *string, catalogNumber *string) (string, error) {
	campus := helpers.GetCampusFromBusinessUnit(institution)
	if academicOrganization == "I-DIST" {
		return "CBE_" + campus + "_" + *subject + "_" + *catalogNumber, nil
	}

	return "", fmt.Errorf("Campus/Academic Organization is not allowed")
}

func Courses(w http.ResponseWriter, r *http.Request) {
	cq := courseQuery{
		Institution:   getQueryParam(r, "campus"),
		Term:          getQueryParam(r, "term"),
		Subject:       getQueryParam(r, "subject"),
		CatalogNumber: getQueryParam(r, "number"),
	}

	errs := validateCourseQuery(cq)
	if len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}

	courses, err := getCourses(cq)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
	} else if courses != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(courses)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func DefunctCourses(w http.ResponseWriter, r *http.Request) {
	cq := courseQuery{
		Institution:   getQueryParam(r, "campus"),
		Term:          getQueryParam(r, "term"),
		Subject:       getQueryParam(r, "subject"),
		CatalogNumber: getQueryParam(r, "number"),
	}

	errs := validateCourseQuery(cq)
	if len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}

	courses, err := getDefunctCourses(cq)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
	} else if courses != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(courses)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}
}

func ChangeNavbars(w http.ResponseWriter, r *http.Request) {
	var cnb rest.CourseNavbar
	err := json.NewDecoder(r.Body).Decode(&cnb)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if errs := db.ValidateCampusAndTerm(cnb.Institution, cnb.Term); len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}
	_, err = strconv.Atoi(cnb.Navbar)
	if err != nil {
		helpers.HttpWriteError(w, r, fmt.Errorf("homepage, %s, is not valid integer", cnb.Navbar))
		return
	}

	courses, err := db.GetAllByCampusAndTerm(cnb.Institution, cnb.Term)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
	} else if courses == nil {
		w.WriteHeader(http.StatusNotFound)
	}

	var navbars []lms.CourseItem
	for _, cc := range courses {
		h := lms.CourseItem{
			Institution: cnb.Institution,
			Term:        cnb.Term,
			CourseLmsId: cc.LmsId,
			ItemLmsId:   cnb.Navbar,
		}

		navbars = append(navbars, h)
	}

	errors := lms.UpdateCourseItemBatch("Navbar", navbars)
	if errors != nil || len(errors) > 0 {
		type marshableError struct {
			Error string `json:"error"`
		}
		var marshableErrors []marshableError
		for _, e := range errors {
			me := marshableError{Error: e.Error()}
			marshableErrors = append(marshableErrors, me)
		}

		helpers.HttpRespond500(w, r, marshableErrors)

		return
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(courses)
}

func ChangeHomepages(w http.ResponseWriter, r *http.Request) {
	var ch rest.CourseHomepage
	err := json.NewDecoder(r.Body).Decode(&ch)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if errs := db.ValidateCampusAndTerm(ch.Institution, ch.Term); len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}
	_, err = strconv.Atoi(ch.Homepage)
	if err != nil {
		helpers.HttpWriteError(w, r, fmt.Errorf("homepage, %s, is not valid integer", ch.Homepage))
		return
	}

	courses, err := db.GetAllByCampusAndTerm(ch.Institution, ch.Term)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
	} else if courses == nil {
		w.WriteHeader(http.StatusNotFound)
	}

	var homepages []lms.CourseItem
	for _, cc := range courses {
		h := lms.CourseItem{
			Institution: ch.Institution,
			Term:        ch.Term,
			CourseLmsId: cc.LmsId,
			ItemLmsId:   ch.Homepage,
		}

		homepages = append(homepages, h)
	}

	errors := lms.UpdateCourseItemBatch("Homepage", homepages)
	if errors != nil || len(errors) > 0 {
		type marshableError struct {
			Error string `json:"error"`
		}
		var marshableErrors []marshableError
		for _, e := range errors {
			me := marshableError{Error: e.Error()}
			marshableErrors = append(marshableErrors, me)
		}

		helpers.HttpRespond500(w, r, marshableErrors)

		return
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(courses)
}

func validateCourseQuery(cq courseQuery) []error {
	var errs []error

	if err := db.ValidateInstitution(cq.Institution); err != nil {
		errs = append(errs, err)
	}

	if err := db.ValidateTerm(cq.Term); err != nil {
		errs = append(errs, err)
	}

	re := regexp.MustCompile(`^[A-Z]{0,4}$`)
	if !re.MatchString(cq.Subject) {
		errs = append(errs, fmt.Errorf("invalid subject, %s", cq.Subject))
	}

	re = regexp.MustCompile(`^[0-9]{0,7}$`)
	if !re.MatchString(cq.CatalogNumber) {
		errs = append(errs, fmt.Errorf("invalid number, %s", cq.CatalogNumber))
	}

	return errs
}

func validateCourseAcadOrg(cao courseAcadOrg) []error {
	var errs []error

	if err := db.ValidateInstitution(cao.Institution); err != nil {
		errs = append(errs, err)
	}

	if cao.Institution != "UMS07" {
		errs = append(errs, fmt.Errorf("selected campus is not allowed to use this feature at this time"))
	}

	if err := db.ValidateTerm(cao.Term); err != nil {
		errs = append(errs, err)
	}

	if cao.AcadOrg != "I-DIST" {
		errs = append(errs, fmt.Errorf("selected academic organization is not allowed to use this feature at this time"))
	}

	return errs
}

func getQueryParam(r *http.Request, param string) string {
	val := r.URL.Query().Get(param)
	val = strings.TrimSpace(val)
	val = strings.ToUpper(val)

	return val
}

func getCourses(cq courseQuery) (courses []db.Course, err error) {
	return getCoursesFromDB(cq, "LMS_COURSE_INFO")
}

func getCoursesByAcadOrg(cao courseAcadOrg) (courses []db.Course, err error) {
	return getCoursesFromDBByAcadOrg(cao, "LMS_COURSE_INFO")
}

func getDefunctCourses(cq courseQuery) (courses []db.Course, err error) {
	return getCoursesFromDB(cq, "LMS_COURSE_INFO_DEFUNCTS")
}

func getCoursesFromDB(cq courseQuery, table string) (courses []db.Course, err error) {
	query := fmt.Sprintf("Select INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE, TITLE, CODE, nvl(INSTRUCTOR_NAMES, '<null>') INSTRUCTOR_NAMES From %s Where TERM = '%s' And INSTITUTION = '%s' And regexp_like(TITLE, '%s[a-z]{0,%d}\\s*%s', 'i')", table, cq.Term, cq.Institution, cq.Subject, 4-len(cq.Subject), cq.CatalogNumber)

	return getCoursesByQuerystring(query)
}

func getCoursesFromDBByAcadOrg(cao courseAcadOrg, table string) (courses []db.Course, err error) {
	query := fmt.Sprintf("Select INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE, TITLE, CODE, nvl(INSTRUCTOR_NAMES, '<null>') INSTRUCTOR_NAMES, LMS_ID From %s Where nvl(LMS_ID, '<null>') <> '<null>' And TERM = '%s' And INSTITUTION = '%s' And ACAD_ORG = '%s'", table, cao.Term, cao.Institution, cao.AcadOrg)

	return getCoursesByQuerystring(query)
}

func getCoursesByQuerystring(query string) (courses []db.Course, err error) {
	dbConn := helpers.DbConn()

	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	results, err := dbConn.Queryx(query)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			courses = nil
			return
		}
		err = fmt.Errorf("query, <<<%s>>>, failed:%v", query, err)
		return
	}
	defer results.Close()

	var c db.Course
	for results.Next() {
		err := results.StructScan(&c)
		if err != nil {
			err = fmt.Errorf("unable to read results with error, %v", err)
			break
		}

		if c.InstructorNames == "<null>" {
			c.InstructorNames = ""
		}
		if c.InstructorEmplids == "<null>" {
			c.InstructorEmplids = ""
		}
		if c.InstructorUsernames == "<null>" {
			c.InstructorUsernames = ""
		}
		if c.Parents == "<null>" {
			c.Parents = ""
		}
		if c.Master == "<null>" {
			c.Master = ""
		}

		courses = append(courses, c)
	}

	return
}

type courseQuery struct {
	Institution   string
	Term          string
	Subject       string
	CatalogNumber string
}

type courseAcadOrg struct {
	Institution string
	Term        string
	AcadOrg     string
}
