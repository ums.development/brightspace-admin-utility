package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func LmsCourseByCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	err := ValidateCourseCode(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	course, err := db.GetCourseByCode(code)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		err = fmt.Errorf("error getting course with error: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	if course.LmsId == "<null>" || len(course.LmsId) == 0 {
		//TODO: Should go try and find the course in the LMS, BSI-83
		w.WriteHeader(http.StatusNotFound)
		return
	}

	lmsId, err := strconv.Atoi(course.LmsId)
	if err != nil {
		err = fmt.Errorf("error converting lmsid with error: %+v", err)
		w.WriteHeader(http.StatusInternalServerError)
		helpers.HttpRespond500(w, r, err)
		return
	}

	lmsCourse, err := lms.GetCourse(lmsId)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	_ = json.NewEncoder(w).Encode(lmsCourse)
}

func PutLmsCourseByCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	err := ValidateCourseCode(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	course, err := db.GetCourseByCode(code)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		err = fmt.Errorf("error getting course: %v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	lmsId, err := strconv.Atoi(course.LmsId)
	if err != nil {
		err = fmt.Errorf("error converting lmsid: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	putCourse := lms.PutCourse{
		Identifier: course.Code,
		Name:       course.Title,
	}
	putCourse, err = lms.UpdateCourse(lmsId, putCourse)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	_ = json.NewEncoder(w).Encode(putCourse)
	w.WriteHeader(http.StatusOK)
}

func LmsCourse(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	course, err := lms.GetCourse(id)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(w).Encode(course)
}

func CreateDevCourse(w http.ResponseWriter, r *http.Request) {
	c := lms.CreateDevCourseStruct{}
	err := json.NewDecoder(r.Body).Decode(&c)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	lmsDevCourse, err, statusCode := lms.CreateDevCourse(c)
	if err != nil {
		http.Error(w, err.Error(), statusCode)
		return
	}

	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(lmsDevCourse)
}

func CreateCourse(w http.ResponseWriter, r *http.Request) {
	var dbCourse db.Course
	var dbCourseCompleted db.CourseCompleted
	var isCourseCompletedExist = true
	err := json.NewDecoder(r.Body).Decode(&dbCourse)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	courseCode := dbCourse.Code

	err = ValidateCourseCode(courseCode)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	dbCourse, err = db.GetCourseByCode(courseCode)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	semesterId, err := CreateSemesterIfNeeded(dbCourse.Term)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	parents := strings.Split(dbCourse.Parents, "|")

	ccs := lms.CreateCourseStruct{
		Name:     dbCourse.Title,
		Code:     courseCode,
		Semester: semesterId,
		Parents:  parents,
		Master:   dbCourse.Master,
	}
	lmsCourse, err := lms.CreateCourse(ccs)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if dbCourse.InstructorUsernames != "<null>" && len(dbCourse.InstructorUsernames) > 0 {
		usernames := strings.Split(dbCourse.InstructorUsernames, ", ")
		_ = enrollInstructors(lmsCourse.Identifier, usernames)
	}
	// update or insert into course_completed table
	dbCourseCompleted.Code = dbCourse.Code
	dbCourseCompleted.Title = dbCourse.Title
	dbCourseCompleted.Term = dbCourse.Term
	dbCourseCompleted.ClassNumber = dbCourse.ClassNumber
	dbCourseCompleted.Institution = dbCourse.Institution
	dbCourseCompleted.IsCombined = dbCourse.IsCombined
	dbCourseCompleted.SessionCode = dbCourse.SessionCode
	dbCourseCompleted.Parents = dbCourse.Parents
	dbCourseCompleted.Master = dbCourse.Master
	dbCourseCompleted.LmsId = lmsCourse.Identifier
	dbCourseCompleted.Semester = ccs.Semester
	dbCourseCompleted.AddedOn = time.Now()
	dbCourseCompleted.ModifiedOn = time.Now()
	isCourseCompletedExist, err = db.IsCourseCompletedExist(dbCourse.Code)
	if err != nil {
		helpers.HttpRespond500(w, r, fmt.Errorf("error select from COURSE_COMPLETED on course code: %+v, with error: %+v", dbCourse.Code, err))
		return
	}
	if isCourseCompletedExist == true {
		// update course_complete if already exist
		err := db.UpdateCourseCompleted(dbCourseCompleted)
		if err != nil {
			helpers.HttpRespond500(w, r, fmt.Errorf("error updating course To COURSE_COMPLETED on terms: %+v, with error: %+v", &dbCourseCompleted, err))
			return

		}
	} else {
		// insert into course_completed table is not exist
		err := db.InsertCourseCompleted(dbCourseCompleted)
		if err != nil {
			err = fmt.Errorf("error updating course To COURSE_COMPLETED on terms: %+v, with error: %+v", &dbCourseCompleted, err)
			helpers.HttpRespond500(w, r, err)
			return
		}
	}
	w.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(w).Encode(lmsCourse)

}

func enrollInstructors(courseLmsId string, usernames []string) []error {
	var errors []error
	for u := range usernames {
		userLmsId, err := db.GetUserLmsId(usernames[u])
		if err != nil {
			errors = append(errors, err)
			continue
		}

		enrollment := lms.Enrollment{
			CourseLmsId: courseLmsId,
			UserLmsId:   userLmsId,
			Type:        "I",
		}
		err = lms.PostEnrollment(enrollment)
		if err != nil {
			errors = append(errors, err)
		}
	}

	return errors
}

func CreateSemesterIfNeeded(semester string) (lmsId string, err error) {
	dbSemester, err := db.GetSemesterByCode(semester)
	if err != nil && err != sql.ErrNoRows {
		return
	}

	if err == sql.ErrNoRows {
		semesterName, err := db.GetSemesterNameByCode(semester)
		if err != nil {
			return "", err
		}

		lmsSemester := lms.Semester{
			Code: semester,
			Name: semesterName,
		}
		semesterId, err := lms.CreateSemester(lmsSemester)
		if err != nil {
			return "", err
		}

		return semesterId, nil
	}

	return dbSemester.LmsId, nil
}
func DeleteCourse(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	err := ValidateCourseCode(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	course, err := db.GetCourseByCode(code)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		err = fmt.Errorf("error getting course with error: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	if course.LmsId == "<null>" || len(course.LmsId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	lmsId, err := strconv.Atoi(course.LmsId)
	if err != nil {
		err = fmt.Errorf("error converting lmsid with error: %+v", err)
		w.WriteHeader(http.StatusInternalServerError)
		helpers.HttpRespond500(w, r, err)
		return
	}

	err = lms.DeleteCourse(lmsId)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}
	err = db.DeleteCourseCompleted(code)
	if err != nil {
		helpers.HttpRespond500(w, r, fmt.Errorf("error delete course from COURSE_COMPLETED by code: %+v, with error: %+v", code, err))

	}
	w.WriteHeader(http.StatusOK)
	return
}
