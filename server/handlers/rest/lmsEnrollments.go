package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
	"net/http"
	"strconv"
)

func LmsEnrollmentsByCode(w http.ResponseWriter, r *http.Request) {
	code := r.URL.Query().Get("code")

	err := ValidateCourseCode(code)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	course, err := db.GetCourseByCode(code)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		err = fmt.Errorf("error getting course: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	lmsId, err := strconv.Atoi(course.LmsId)
	if err != nil {
		err = fmt.Errorf("error converting lmsid: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	enrollments, err := lms.GetEnrollmentsByCourse(lmsId)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	_ = json.NewEncoder(w).Encode(enrollments)
}
