package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
	"net/http"
	"strconv"
)

func GetLmsUserByEmplid(w http.ResponseWriter, r *http.Request) {
	emplid := r.URL.Query().Get("emplid")

	err := validateEmplid(emplid)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	userLmsId, err := db.GetUserLmsIdByEmplid(emplid)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		return
	} else if err != nil {
		err = fmt.Errorf("error getting user with error: %+v", err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	if userLmsId == "<null>" || len(userLmsId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	lmsId, err := strconv.Atoi(userLmsId)
	if err != nil {
		err = fmt.Errorf("error converting lmsid with error: %+v", err)
		w.WriteHeader(http.StatusInternalServerError)
		helpers.HttpRespond500(w, r, err)
		return
	}

	lmsUser, err := lms.GetUser(lmsId)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	_ = json.NewEncoder(w).Encode(lmsUser)
}

func PutLmsUserByEmplid(w http.ResponseWriter, r *http.Request) {
	var user lms.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	err = validateEmplid(user.OrgDefinedId)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}
	// todo: validate other element?
	userId, err := db.GetUserLmsIdByEmplid(user.OrgDefinedId)
	if err != nil {
		helpers.HttpRespond403(w)
		return
	}
	userLmsId, err := strconv.Atoi(userId)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}
	var putUser = lms.PutUser{
		FirstName:     user.FirstName,
		MiddleName:    user.MiddleName,
		LastName:      user.LastName,
		UserName:      user.UserName,
		ExternalEmail: user.ExternalEmail,
		OrgDefinedId:  user.OrgDefinedId,
		PrimaryCampus: "",
		Campuses:      []string{},
	}
	putUser, err = lms.UpdateUser(userLmsId, putUser)
	if err != nil {
		helpers.HttpRespond500(w, r, err)
		return
	}

	_ = json.NewEncoder(w).Encode(user)
	w.WriteHeader(http.StatusOK)
}
