package rest

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"gitlab.its.maine.edu/development/lms/utility/server/models/lms"
	"gitlab.its.maine.edu/development/lms/utility/server/models/rest"
	"net/http"
	"strconv"
)

func GetAllRoleOverrides(w http.ResponseWriter, r *http.Request) {
	roleOverrides, err := rest.GetAllRoleOverrides()
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if roleOverrides != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(roleOverrides)
		return
	}

	w.WriteHeader(http.StatusNotFound)
}

func GetAllRoles(w http.ResponseWriter, r *http.Request) {
	roles, err := lms.GetAllRoles()
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if roles != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(roles)
		return
	}

	w.WriteHeader(http.StatusNotFound)
}

func DeleteRole(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userLmsId, err := strconv.ParseInt(vars["userLmsId"], 10, 64)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	loggedInUser := helpers.GetLoggedInUser(r)
	loggedInUserLmsId, err := db.GetUserLmsId(loggedInUser)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if loggedInUserLmsId == vars["userLmsId"] {
		helpers.HttpRespond403(w)
		return
	}

	err = db.DeleteRoleOverride(strconv.FormatInt(userLmsId, 10))
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}

func AddRole(w http.ResponseWriter, r *http.Request) {
	var roleOverride db.RoleOverride
	err := json.NewDecoder(r.Body).Decode(&roleOverride)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	loggedInUser := helpers.GetLoggedInUser(r)
	loggedInUserLmsId, err := db.GetUserLmsId(loggedInUser)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if loggedInUserLmsId == roleOverride.UserLmsId {
		helpers.HttpRespond403(w)
		return
	}

	exists, err := db.DoesUserHaveAnOveriddenRole(roleOverride.UserLmsId)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if exists {
		err = db.DeleteRoleOverride(roleOverride.UserLmsId)
		if err != nil {
			helpers.HttpWriteError(w, r, err)
			return
		}
	}

	err = db.AddRoleOverride(roleOverride)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	w.WriteHeader(http.StatusOK)
	return
}
