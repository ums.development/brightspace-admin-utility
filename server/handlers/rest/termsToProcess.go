package rest

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
)

func TermsToProcessGet(w http.ResponseWriter, r *http.Request) {
	terms, err := getTerms()
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if terms != nil {
		w.WriteHeader(http.StatusOK)
		_ = json.NewEncoder(w).Encode(terms)
	} else {
		w.WriteHeader(http.StatusNotFound)
	}

}

func getTerms() (terms []db.TermsToProcess, err error) {
	dbConn := helpers.DbConn()
	query := "Select INSTITUTION, TERM, CAMPUS_NAME, CAMPUS_SHORT_NAME, TERM_NAME, PK From LMS_TERMS_TO_PROCESS"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Select(&terms, query)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		if err == sql.ErrNoRows {
			err = nil
			terms = nil
			return
		}

		err = fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	return
}

func TermsToProcessDelete(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	campus := params["campus"]
	term := params["term"]

	if errs := db.ValidateCampusAndTerm(campus, term); len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}

	ttp := db.TermsToProcess{
		Institution: campus,
		Term:        term,
	}

	dbConn := helpers.DbConn()
	query := "Delete From TERMS_TO_PROCESS Where INSTITUTION = :INSTITUTION And TERM = :TERM"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	sqlResult, err := dbConn.NamedExec(query, &ttp)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		err = fmt.Errorf("error deleting Terms To Process with terms: %+v, and error: %+v", &ttp, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("delete from TERMS_TO_PROCESS error %d rows affected for terms: %+v, and error: %+v", rowsAffected, &ttp, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	if rowsAffected == 1 {
		w.WriteHeader(http.StatusOK)
		return
	}

	err = fmt.Errorf("delete from TERMS_TO_PROCESS unexpected %d rows affected for terms: %+v, and error: %+v", rowsAffected, &ttp, err)
	helpers.HttpRespond500(w, r, err)
}

func TermsToProcessAdd(w http.ResponseWriter, r *http.Request) {
	var ttp db.TermsToProcess
	err := json.NewDecoder(r.Body).Decode(&ttp)
	if err != nil {
		helpers.HttpWriteError(w, r, err)
		return
	}

	if errs := db.ValidateCampusAndTerm(ttp.Institution, ttp.Term); len(errs) > 0 {
		helpers.HttpWriteError(w, r, errs)
		return
	}

	dbConn := helpers.DbConn()
	query := "Select count(*) ROWCOUNT From TERMS_TO_PROCESS Where INSTITUTION=:INSTITUTION And TERM=:TERM"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	row, err := dbConn.NamedQuery(query, &ttp)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		err = fmt.Errorf("error checking if campus/term exists on adding Terms To Process on terms: %+v, with error: %+v", &ttp, err)
		helpers.HttpRespond500(w, r, err)
		return
	}
	var count int
	if !row.Next() {
		err = fmt.Errorf("error getting count of campus/term when adding Terms To Process on terms: %+v, with error: %+v", &ttp, err)
		helpers.HttpRespond500(w, r, err)
		return
	}
	err = row.Scan(&count)
	if err != nil {
		err = fmt.Errorf("error counting campus/term when adding Terms To Process on terms: %+v, with error: %+v", &ttp, err)
		helpers.HttpRespond500(w, r, err)
		return
	}
	if count > 0 {
		helpers.Logger().Infow("Campus/Term already exists", "termsToProcess", &ttp)
		w.WriteHeader(http.StatusConflict)
		fmt.Fprintf(w, "Campus/Term already exists")
		err = fmt.Errorf("campus/Term already exists for terms: %+v", &ttp)
		helpers.HttpWriteErrorWithCode(w, r, err, http.StatusConflict)

		return
	}

	insertQuery := "Insert Into TERMS_TO_PROCESS (INSTITUTION, TERM) Values (:INSTITUTION, :TERM)"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", insertQuery))
	sqlResult, err := dbConn.NamedExec(insertQuery, &ttp)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", insertQuery))
	if err != nil {
		err = fmt.Errorf("error adding Terms To Process on terms: %+v, with error: %+v", &ttp, err)
		helpers.HttpRespond500(w, r, err)
		return
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		err = fmt.Errorf("add to TERMS_TO_PROCESS error %d rows affected on terms: %+v, with error: %+v", rowsAffected, &ttp, err.Error())

		helpers.HttpWriteError(w, r, err)
		return
	}

	if rowsAffected == 1 {
		w.WriteHeader(http.StatusOK)
		return
	}

	err = fmt.Errorf("add to TERMS_TO_PROCESS unexpected %d rows affected on terms: %+v, with error: %+v", rowsAffected, &ttp, err)
	helpers.HttpRespond500(w, r, err)
}
