package handlers

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"net/http"
	"os"
	"path/filepath"
	"runtime"
)

type VueHandler struct {
	StaticPath string
	IndexPath  string
}

func (h VueHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// get the absolute path to prevent directory traversal
	path, err := filepath.Abs(r.URL.Path)
	// strip drive letter on Windows
	if runtime.GOOS == "windows" {
		path = path[3:]
	}
	helpers.Logger().Debugw(fmt.Sprintf("Resolved url path %s to file path %s", r.URL.Path, path))
	if err != nil {
		// if we failed to get the absolute path respond with a 400 bad request
		// and stop
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	path = filepath.Join(h.StaticPath, path)
	helpers.Logger().Debugw(fmt.Sprintf("Resolve url path %s to static file path %s", r.URL.Path, path))

	// check whether a file exists at the given path
	_, err = os.Stat(path)
	if os.IsNotExist(err) {
		helpers.Logger().Debugw(fmt.Sprintf("File not found to url path %s", r.URL.Path))
		// file does not exist, serve index
		http.ServeFile(w, r, filepath.Join(h.StaticPath, h.IndexPath))
		return
	} else if err != nil {
		helpers.Logger().Errorf("Error serving url path %s", r.URL.Path)
		// if we got an error (that wasn't that the file doesn't exist) stating the
		// file, return a 500 internal server error and stop
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	helpers.Logger().Debugw(fmt.Sprintf("Serving static file %s", path))

	http.FileServer(http.Dir(h.StaticPath)).ServeHTTP(w, r)
}
