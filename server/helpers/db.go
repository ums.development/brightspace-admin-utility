package helpers

import (
	"github.com/jmoiron/sqlx"
	"github.com/mattn/go-oci8"
	"log"
	"os"
	"sync"
)

var doOnce sync.Once
var connDB *sqlx.DB

func DbConn() *sqlx.DB {
	doOnce.Do(func() {
		oci8.Driver.Logger = log.New(os.Stderr, "oci8 ", log.Ldate|log.Ltime|log.LUTC|log.Lshortfile)

		user := os.Getenv("DB_USER")
		pass := os.Getenv("DB_PASSWORD")
		host := os.Getenv("DB_HOST")
		port := os.Getenv("DB_PORT")
		sid := os.Getenv("DB_SID")

		var err error = nil
		connDB, err = sqlx.Connect("oci8", user+"/"+pass+"@"+host+":"+port+"/"+sid)
		if err != nil {
			logger.Errorf("Error opening database: %v", err)
		}
	})

	return connDB
}
