package middleware

import (
	"errors"
	"net/http"
)

type Auth interface {
	Init(connection string) error
	AuthHandler(handler http.Handler) http.Handler
	GetUser(r *http.Request) string
}

func CreateAuth(authType string) (Auth, error) {
	switch authType {
	case "NOAUTH":
		return new(NoAuth), nil
	case "CAS":
		return new(CasAuth), nil
	default:
		return nil, errors.New("invalid auth type")
	}
}
