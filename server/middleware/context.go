package middleware

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"io"
	"net/http"
	"sync"
)

type SessionData struct {
	userId string
	adminUser db.AdminUser
	adminPerms []db.AdminPermission
}

type SessionStore struct {
	SessionData map[string]SessionData
	lock        sync.Mutex
}

func Context(r *mux.Router, auth Auth, store *sessions.CookieStore, sessionStore *SessionStore) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			rid := r.Context().Value("rid")

			helpers.Logger().Debugw("Attempting to lock session store", "rid", rid)
			sessionStore.lock.Lock()
			helpers.Logger().Debugw("Session store locked", "rid", rid)

			session, err := store.Get(r, "bsi-session")
			if err != nil {
				helpers.Logger().Debugw("Session store unlocked", "rid", rid)
				sessionStore.lock.Unlock()
				helpers.Logger().Errorf("Error finding session: %s", err.Error())
				helpers.HttpRespond403(w)
				return
			}

			sessionId, sessionIdExists := session.Values["id"].(string)

			if !sessionIdExists {
				sessionId = generateSessionId()
				session.Values["id"] = sessionId
			}

			authUser := auth.GetUser(r)
			sessionData, sessionDataExists := sessionStore.SessionData[sessionId]
			if !sessionDataExists {
				helpers.Logger().Debugw(fmt.Sprintf("Creating new session for user %s", authUser), "rid", rid)
				sessionData = SessionData{
					userId: authUser,
				}
			} else {
				helpers.Logger().Debugw(fmt.Sprintf("Found session for user %s", authUser), "rid", rid)
			}

			err = session.Save(r, w)
			if err != nil {
				helpers.Logger().Debugw("Session store unlocked", "rid", rid)
				sessionStore.lock.Unlock()
				helpers.Logger().Infof("Error saving session for user %s: %s", sessionData.userId, err.Error())
				helpers.HttpRespond403(w)
				return
			}

			if sessionData.adminUser.Id == 0 {
				var dbAdminUser db.AdminUser
				helpers.Logger().Debugw("Start admin user lookup", "rid", rid)
				dbAdminUser, err = db.GetAdminUser(sessionData.userId)
				if err != nil {
					helpers.Logger().Debugw("Session store unlocked", "rid", rid)
					sessionStore.lock.Unlock()
					helpers.Logger().Infof("Error finding admin user for %s: %s", sessionData.userId, err.Error())
					helpers.HttpRespond403(w)
					return
				}
				sessionData.adminUser = dbAdminUser
				helpers.Logger().Debugw("Completed admin user lookup", "rid", rid)
			}

			if sessionData.adminPerms == nil {
				var dbAdminPerms []db.AdminPermission
				helpers.Logger().Debugw(fmt.Sprintf("Start permission lookup for user %s", sessionData.userId), "rid" , rid)
				dbAdminPerms, err = db.GetAllowedPermissions(sessionData.adminUser)
				if err != nil {
					helpers.Logger().Debugw("Session store unlocked", "rid", rid)
					sessionStore.lock.Unlock()
					helpers.Logger().Infof("Error finding admin user permissions for %s: %s", sessionData.userId, err.Error())
					helpers.HttpRespond403(w)
					return
				}
				sessionData.adminPerms = dbAdminPerms
				helpers.Logger().Debugw(fmt.Sprintf("Completed permission lookup for user %s", sessionData.userId), "rid" , rid)
			}

			sessionStore.SessionData[sessionId] = sessionData

			helpers.Logger().Debugw("Session store unlocked", "rid", rid)
			sessionStore.lock.Unlock()

			ctx := context.WithValue(r.Context(), "userId", sessionData.userId)
			ctx = context.WithValue(ctx, "adminUser", sessionData.adminUser)
			ctx = context.WithValue(ctx, "adminPerms", sessionData.adminPerms)

			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

func CreateNewSessionDataStore() *SessionStore {
	return &SessionStore{
		SessionData: make(map[string]SessionData),
	}
}

func GetSessionData(r *http.Request, store *sessions.CookieStore, sessionStore *SessionStore) *SessionData {
	session, err := store.Get(r, "bsi-session")
	if err != nil {
		return nil;
	}
	sessionId, _ := session.Values["id"].(string)
	sessionData, _ := sessionStore.SessionData[sessionId]
	return &sessionData
}

func generateSessionId() string {
	b := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return ""
	}
	return base64.URLEncoding.EncodeToString(b)
}
