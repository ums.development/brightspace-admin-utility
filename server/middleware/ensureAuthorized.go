package middleware

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
	"os"
	"strconv"
	"strings"
)

func EnsureAuthorized(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		rid := r.Context().Value("rid")

		helpers.Logger().Debugw("Start user lookup", "rid", rid)
		userId := helpers.GetLoggedInUser(r)
		helpers.Logger().Debugw(fmt.Sprintf("Completed user lookup %s", userId), "rid", rid)
		adminUser := r.Context().Value("adminUser").(db.AdminUser)

		if !adminUser.IsActive {
			helpers.Logger().Infof("User, %s, is not active", adminUser.Username)
			helpers.HttpRespond403(w)
			return
		}

		path := r.URL.Path
		context := os.Getenv("APP_CONTEXT")
		if len(context) > 0 {
			path = strings.Replace(path, context, "", 1)
		}

		perms := r.Context().Value("adminPerms").([]db.AdminPermission)
		hasPermission, err := hasPermissionToAccessRoute(adminUser, path, r.Method, perms)
		helpers.Logger().Debugw(fmt.Sprintf("Completed permission lookup for user %s on path %s", adminUser.Username, path), "rid" , rid, "hasPermission", hasPermission)
		if err != nil {
			helpers.HttpRespond500(w, r, err)
			return
		}

		if hasPermission {
			next.ServeHTTP(w, r)
			return
		}

		helpers.HttpRespond403(w)
		return
	})
}

func hasPermissionToAccessRoute(adminUser db.AdminUser, path string, method string, perms []db.AdminPermission) (bool, error) {

	if len(perms) <= 0 {
		return false, nil
	}

	for _, perm := range perms {
		if strings.ToLower(perm.Action) == strings.ToLower(method) {
			path := strings.ToLower(path)
			expected := strings.ToLower(perm.Route)
			if path == expected {
				return true, nil
			}

			if doesPathMatchPrefix(path, expected) {
				return true, nil
			}

			if doesPathMatchWithParameters(path, expected) {
				return true, nil
			} else {
				continue
			}
		}
	}

	return false, nil
}

func doesPathMatchPrefix(pathVisited string, pathToCheck string) bool {
	if !strings.HasSuffix(pathToCheck, "/**") {
		return false
	}

	pathWithoutGlob := strings.ReplaceAll(pathToCheck, "**", "")
	if strings.HasPrefix(pathVisited, pathWithoutGlob) {
		return true
	}

	return false
}

func doesPathMatchWithParameters(pathVisited string, pathToCheck string) bool {
	paths := strings.Split(pathVisited, "/")
	expecteds := strings.Split(pathToCheck, "/")

	if len(paths) != len(expecteds) {
		return false
	}

	for i, e := range expecteds {
		if paths[i] == e {
			continue
		}

		if strings.Contains(e, "{") {
			if strings.Contains(e, ":[0-9]+") {
				if _, err := strconv.Atoi(paths[i]); err != nil {
					return false
				}
			}

			if len(paths[i]) <= 0 {
				return false
			}
		} else {
			return false
		}
	}

	return true
}
