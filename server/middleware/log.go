package middleware

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"io"
	"io/ioutil"
	"net/http"
)

func Log(store *sessions.CookieStore, sessionStore *SessionStore) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			rid := r.Context().Value("rid")

			bodyBytes, _ := ioutil.ReadAll(r.Body)
			_ = r.Body.Close()
			r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))

			jsonBody := make(map[string]interface{})
			err := json.NewDecoder(bytes.NewReader(bodyBytes)).Decode(&jsonBody)

			helpers.Logger().Debugw(fmt.Sprintf("Received request %s %s", r.Method, r.RequestURI), "rid", rid)

			next.ServeHTTP(w, r)

			sessionData := GetSessionData(r, store, sessionStore)
			var user string;
			if sessionData != nil {
				user = sessionData.userId
			}

			if err != nil && err != io.EOF {
				helpers.Logger().Infow("User Route Accessed",
					"rid", rid,
					"user", user,
					"method", r.Method,
					"route", r.RequestURI,
					"body", string(bodyBytes),
					"error", "error reading request body",
					"bodyError", err)
			} else {
				helpers.Logger().Infow("User Route Accessed",
					"rid", rid,
					"user", user,
					"method", r.Method,
					"route", r.RequestURI,
					"body", jsonBody)
			}
		})
	}
}
