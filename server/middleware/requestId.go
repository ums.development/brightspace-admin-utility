package middleware

import (
	"context"
	"github.com/gorilla/mux"
	"net/http"
	"sync"
)

type SyncCounter struct {
	mutex sync.Mutex
	lastId int
}

func RequestId(counter *SyncCounter) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		counter.mutex.Lock()
		counter.lastId++
		rid := counter.lastId
		counter.mutex.Unlock()
		ctx := context.WithValue(r.Context(), "rid", rid)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
	}
}
