package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strings"
)

type AdminUser struct {
	Username                string            `json:"username" db:"USERNAME"`
	Id                      int64             `json:"id" db:"ID"`
	ShowAdminHelpInfo       bool              `json:"showAdminHelpInfo" db:"SHOW_ADMIN_HELP_INFO"`
	IsActive                bool              `json:"isActive" db:"IS_ACTIVE"`
	AllowedAdminPages       []AdminPage       `json:"allowedAdminPages,omitempty"`
	AllowedAdminPermissions []AdminPermission `json:"allowedAdminPermissions,omitempty"`
}

func GetAdminUserById(id int64, withAllowedPages bool, withAllowedPermissions bool) (AdminUser, error) {
	dbConn := helpers.DbConn()
	var users []AdminUser
	query := "Select ID, USERNAME, SHOW_ADMIN_HELP_INFO, IS_ACTIVE From ADMIN_USER Where ID = :ID"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err := dbConn.Select(&users, query, id)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		return AdminUser{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)

	}

	if len(users) == 0 {
		return AdminUser{}, fmt.Errorf("user id, %v, not found", id)
	}

	user := users[0]

	if withAllowedPages {
		pages, err := GetAllowedPages(user)
		if err != nil {
			return user, err
		}
		user.AllowedAdminPages = pages
	}

	if withAllowedPermissions {
		perms, err := GetAllowedPermissions(user)
		if err != nil {
			return user, err
		}
		user.AllowedAdminPermissions = perms
	}

	return user, nil
}

func GetAdminUser(username string) (AdminUser, error) {
	dbConn := helpers.DbConn()
	var users []AdminUser
	query := "Select ID, USERNAME, SHOW_ADMIN_HELP_INFO, IS_ACTIVE From ADMIN_USER Where USERNAME = :USERNAME"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err := dbConn.Select(&users, query, username)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		return AdminUser{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)

	}

	if len(users) == 0 {
		return AdminUser{}, fmt.Errorf("user, %s, not found", username)
	}

	return users[0], nil
}

func GetAllAdminUsers() ([]AdminUser, error) {
	dbConn := helpers.DbConn()
	var users []AdminUser
	query := "Select ID, USERNAME, SHOW_ADMIN_HELP_INFO, IS_ACTIVE From ADMIN_USER"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err := dbConn.Select(&users, query)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		return []AdminUser{}, fmt.Errorf("query, <<<%s>>>, failed: %v", query, err)
	}

	if len(users) == 0 {
		return []AdminUser{}, fmt.Errorf("no active users")
	}

	return users, nil
}

func ValidateAdminUser(adminUser AdminUser, requireId bool) error {
	if len(adminUser.Username) <= 0 {
		return fmt.Errorf("invalid username")
	}

	if adminUser.Id <= 0 && requireId {
		return fmt.Errorf("invalid id")
	}

	return nil
}

func IsUserSame(adminUserId int64, username string) (bool, error) {
	adminUser, err := GetAdminUserById(adminUserId, false, false)
	if err != nil {
		return false, err
	}

	if strings.ToLower(adminUser.Username) == strings.ToLower(username) {
		return true, nil
	}

	return false, nil
}
