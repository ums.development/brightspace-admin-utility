package db

import (
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type AdminUserPage struct {
	Id          int64 `json:"id,omitempty" db:"ID"`
	AdminUserId int64 `json:"adminUserId,omitempty" db:"ADMIN_USER_ID"`
	AdminPageId int64 `json:"adminPageId,omitempty" db:"ADMIN_PAGE_ID"`
}

func AdminUserPageAdd(adminUserPage AdminUserPage) error {
	_, err := helpers.DbConn().NamedExec("Insert Into ADMIN_USER_PAGE (ADMIN_USER_ID, ADMIN_PAGE_ID) Values (:ADMIN_USER_ID, :ADMIN_PAGE_ID)", &adminUserPage)
	if err != nil {
		return fmt.Errorf("error adding admin user page with error: %+v", err)
	}

	return AddAdminUserPermissionsByPage(adminUserPage)
}

func AdminUserPageDelete(adminUserPage AdminUserPage) error {
	_, err := helpers.DbConn().NamedExec("Delete From ADMIN_USER_PAGE Where ADMIN_USER_ID=:ADMIN_USER_ID And ADMIN_PAGE_ID=:ADMIN_PAGE_ID", &adminUserPage)
	if err != nil {
		return fmt.Errorf("error removing admin user page with error: %+v", err)
	}

	return DeleteAdminUserPermissionsByPage(adminUserPage)
}

func DoesAdminUserPageExist(adminUserPage AdminUserPage) (bool, error) {
	dbConn := helpers.DbConn()
	query := "Select count(*) ROWCOUNT From ADMIN_USER_PAGE Where ADMIN_USER_ID=:ADMIN_USER_ID And ADMIN_PAGE_ID=:ADMIN_PAGE_ID"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	row, err := dbConn.NamedQuery(query, &adminUserPage)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		return false, fmt.Errorf("error checking if user page already exists with error: %+v", err)
	}

	var count int
	if !row.Next() {
		return false, fmt.Errorf("error getting count of user pages with error: %+v", err)
	}

	err = row.Scan(&count)
	if err != nil {
		return false, fmt.Errorf("error counting user page adding admin user with error: %+v", err)
	}

	return count > 0, nil
}
