package db

import (
	"database/sql"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"regexp"
	"strconv"
	"strings"
)

type Course struct {
	Institution         string `json:"institution" db:"INSTITUTION"`
	Term                string `json:"term" db:"TERM"`
	ClassNumber         string `json:"classNumber" db:"CLASS_NUMBER"`
	IsCombined          string `json:"isCombined" db:"IS_COMBINED"`
	SessionCode         string `json:"sessionCode" db:"SESSION_CODE"`
	Title               string `json:"title" db:"TITLE"`
	Code                string `json:"code" db:"CODE"`
	InstructorNames     string `json:"instructorNames" db:"INSTRUCTOR_NAMES"`
	InstructorEmplids   string `json:"instructorEmplids" db:"INSTRUCTOR_EMPLIDS"`
	InstructorUsernames string `json:"instructorUsernames" db:"INSTRUCTOR_USERNAMES"`
	LmsId               string `json:"lmsId" db:"LMS_ID"`
	Parents             string `json:"parents" db:"PARENTS"`
	Master              string `json:"master" db:"MASTER"`
	Subject             *string `json:"subject" db:"SUBJECT"`
	CatalogNumber       *string `json:"catalogNumber" db:"CATALOG_NBR"`
}

func GetCourseByCode(code string) (course Course, err error) {
	dbConn := helpers.DbConn()

	dbCourse := ParseCourseCode(code)
	query := "Select INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE, TITLE, CODE, nvl(LMS_ID, '<null>') LMS_ID, nvl(INSTRUCTOR_NAMES, '<null>') INSTRUCTOR_NAMES, nvl(INSTRUCTOR_EMPLIDS, '<null>') INSTRUCTOR_EMPLIDS, nvl(INSTRUCTOR_USERNAMES, '<null>') INSTRUCTOR_USERNAMES, nvl(PARENTS, '<null>') PARENTS, nvl(MASTER, '<null>') MASTER, SUBJECT, CATALOG_NBR From LMS_COURSE_INFO_ALL Where INSTITUTION=:1 And TERM=:2 And CLASS_NUMBER=:3 And IS_COMBINED=:4 And SESSION_CODE=:5"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Get(&course, query, dbCourse.Institution, dbCourse.Term, dbCourse.ClassNumber, dbCourse.IsCombined, dbCourse.SessionCode)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))

	if err == sql.ErrNoRows {
		return
	} else if err != nil {
		err = fmt.Errorf("query to get course by code failed:%v", err)
		return
	}

	return
}

func GetCourseById(lmsId string) (course Course, err error) {
	dbConn := helpers.DbConn()

	query := "Select INSTITUTION, TERM, CLASS_NUMBER, IS_COMBINED, SESSION_CODE, TITLE, CODE, nvl(LMS_ID, '<null>') LMS_ID, nvl(INSTRUCTOR_NAMES, '<null>') INSTRUCTOR_NAMES, nvl(INSTRUCTOR_EMPLIDS, '<null>') INSTRUCTOR_EMPLIDS, nvl(INSTRUCTOR_USERNAMES, '<null>') INSTRUCTOR_USERNAMES, nvl(PARENTS, '<null>') PARENTS, nvl(MASTER, '<null>') MASTER, SUBJECT, trim(CATALOG_NBR) CATALOG_NBR From LMS_COURSE_INFO_ALL Where LMS_ID=:1"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Get(&course, query, lmsId)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))

	if err == sql.ErrNoRows {
		return
	} else if err != nil {
		err = fmt.Errorf("query to get course by id failed:%v", err)
		return
	}

	return
}

func ParseCourseCode(code string) Course {
	c := Course{}

	dots := strings.Split(code, ".")
	dashes := strings.Split(dots[1], "-")

	isCombined := "n"
	if dashes[1] == "C" {
		isCombined = "y"
	}

	c.Code = code
	c.Term = dots[0]
	c.Institution = dashes[0]
	c.ClassNumber = dots[2]
	c.IsCombined = isCombined
	c.SessionCode = dots[3]

	return c
}

func ValidateCampusAndTerm(campus string, term string) []error {
	var errs []error

	if err := ValidateInstitution(campus); err != nil {
		errs = append(errs, err)
	}

	if err := ValidateTerm(term); err != nil {
		errs = append(errs, err)
	}

	return errs
}

func ValidateTerm(term string) error {
	t, err := strconv.Atoi(term)
	if err != nil {
		return fmt.Errorf("term, %s, is not valid integer", term)
	}

	if t < 1110 || t > 9930 {
		return fmt.Errorf("term, %s, is out of range", term)
	}

	return nil
}

func ValidateInstitution(campus string) error {
	re := regexp.MustCompile(`^UMS0[1-7]$`)
	if !re.MatchString(campus) {
		return fmt.Errorf("invalid campus, %s", campus)
	}

	return nil
}
