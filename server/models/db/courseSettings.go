package db

import (
	"database/sql"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type CourseSettings struct {
	Institution         string `json:"campus" db:"INSTITUTION"`
	IsActive            bool   `json:"isActive" db:"IS_ACTIVE"`
	SetStartDate        bool   `json:"setStartDate" db:"SET_START_DATE"`
	StartDateDaysOffset int    `json:"startDateOffset" db:"START_DATE_DAYS_OFFSET"`
	SetEndDate          bool   `json:"setEndDate" db:"SET_END_DATE"`
	EndDateDaysOffset   int    `json:"endDateOffset" db:"END_DATE_DAYS_OFFSET"`
}

func GetCourseSettings(campus string) (courseSettings CourseSettings, err error) {
	dbConn := helpers.DbConn()

	query := "Select INSTITUTION, IS_ACTIVE, SET_START_DATE, START_DATE_DAYS_OFFSET, SET_END_DATE, END_DATE_DAYS_OFFSET From COURSE_SETTINGS Where INSTITUTION=:1"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Get(&courseSettings, query, campus)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))

	if err == sql.ErrNoRows {
		return
	} else if err != nil {
		err = fmt.Errorf("query to get course by code failed:%v", err)
		return
	}

	return
}

func UpdateCourseSettings(cs CourseSettings) error {
	dbConn := helpers.DbConn()

	query := "UPDATE COURSE_SETTINGS SET IS_ACTIVE=:IS_ACTIVE, SET_START_DATE=:SET_START_DATE, START_DATE_DAYS_OFFSET=:START_DATE_DAYS_OFFSET, SET_END_DATE=:SET_END_DATE, END_DATE_DAYS_OFFSET=:END_DATE_DAYS_OFFSET Where INSTITUTION=:INSTITUTION"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	sqlResult, err := dbConn.NamedExec(query, &cs)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		return fmt.Errorf("query to update course settings failed: %v", err)
	}

	rowsAffected, err := sqlResult.RowsAffected()
	if err != nil {
		return fmt.Errorf("unable to get the rows affected with error %v", err.Error())
	}

	if rowsAffected != 1 {
		return fmt.Errorf("updated %d rows, but should have only updated 1", rowsAffected)
	}

	return nil
}
