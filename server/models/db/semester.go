package db

import (
	"database/sql"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type Semester struct {
	Code  string `json:"code" db:"CODE"`
	Name  string `json:"name" db:"NAME"`
	LmsId string `json:"lms_id" db:"LMS_ID"`
}

func GetSemesterByCode(code string) (semester Semester, err error) {
	dbConn := helpers.DbConn()
	query := "Select CODE, NAME, LMS_ID From SEMESTER_COMPLETED Where rownum < 2 And Code = :1"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Get(&semester, query, code)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		if err == sql.ErrNoRows {
			return
		}

		err = fmt.Errorf("query, <<<%s>>>, failed: %+v", query, err)
		return
	}

	return
}

func GetSemesterNameByCode(code string) (name string, err error) {
	dbConn := helpers.DbConn()
	query := "Select NAME From PS_SEMESTER Where rownum < 2 And Term = :1"
	helpers.Logger().Debugw(fmt.Sprintf("Start query %s", query))
	err = dbConn.Get(&name, query, code)
	helpers.Logger().Debugw(fmt.Sprintf("Completed query %s", query))
	if err != nil {
		err = fmt.Errorf("query, <<<%s>>>, failed: %+v", query, err)
		return
	}

	return
}
