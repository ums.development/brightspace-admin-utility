package lms

import (
	"bufio"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"io"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

type CourseItem struct {
	Institution string
	Term        string
	CourseLmsId string
	ItemLmsId   string
}

func UpdateCourseItemBatch(itemType string, courseItems []CourseItem) []error {
	var errors []error
	client, err := loginToLMS(itemType)
	if err != nil {
		return append(errors, err)
	}

	for _, h := range courseItems {
		e := updateCourseItem(client, itemType, h)
		if e != nil {
			errors = append(errors, e)
		}
	}

	if len(errors) > 0 {
		return errors
	}

	return nil
}

func updateCourseItem(client *http.Client, itemType string, courseItem CourseItem) error {
	d2lReferrerId, currentItemId, err := getCurrentCourseItemMetaData(client, itemType, courseItem)
	if err != nil {
		return err
	}

	itemLmsId, err := strconv.Atoi(courseItem.ItemLmsId)
	if err != nil {
		return err
	}

	data := url.Values{}
	data.Set("isXhr", "true")
	data.Set("requestId", "2")
	data.Set(fmt.Sprintf("Active%sId", itemType), strconv.Itoa(itemLmsId))
	data.Set("d2l_referrer", d2lReferrerId)

	_, statusCode := httpRequest(client, "POST", fmt.Sprintf("d2l/lp/%ss/%s/setActive", strings.ToLower(itemType), courseItem.CourseLmsId), strings.NewReader(data.Encode()))
	if statusCode >= 400 {
		helpers.Logger().Errorw("unable to update course",
			"itemType", itemType,
			"courseItem", courseItem)
		return fmt.Errorf("failure to update course, %s, to the courseItem, %s", courseItem.CourseLmsId, courseItem.ItemLmsId)
	}
	helpers.Logger().Infow("updated course",
		"itemType", itemType,
		"courseItem", courseItem,
		"currentItemId", currentItemId)

	return nil
}

func getCurrentCourseItemMetaData(client *http.Client, itemType string, courseItem CourseItem) (d2lReferrer string, currentLmsId string, error error) {
	currentLmsId = "unknown"

	body, statusCode := httpRequest(client, "GET", fmt.Sprintf("d2l/lp/%ss/%s/list", strings.ToLower(itemType), courseItem.CourseLmsId), nil)
	if statusCode >= 400 {
		helpers.Logger().Errorw("unable to get course page listing",
			"itemType", itemType,
			"courseItem", courseItem)
		return "", "", fmt.Errorf("failure to get course %s listing for %s, which is needed in order to get the XSRF id", itemType, courseItem.CourseLmsId)
	}
	helpers.Logger().Debugw("obtained course page listing",
		"itemType", itemType,
		"courseItem", courseItem)

	scanner := bufio.NewScanner(body)
	xsrfLine := ""
	activeItemValueLine := ""
	lookingForActiveItemDDLSelectedOption := false
	for scanner.Scan() {
		if xsrfLine == "" {
			if strings.Contains(scanner.Text(), "D2L.LP.Web.Authentication.Xsrf.Init") {
				xsrfLine = scanner.Text()
				break
			}
		}
		if activeItemValueLine == "" {
			if strings.Contains(scanner.Text(), fmt.Sprintf("name=\"Active%sId\"", itemType)) {
				lookingForActiveItemDDLSelectedOption = true
			}
		}
		if lookingForActiveItemDDLSelectedOption {
			if strings.Contains(scanner.Text(), "\" selected>") {
				activeItemValueLine = scanner.Text()
				lookingForActiveItemDDLSelectedOption = false
			}
		}
	}
	if xsrfLine == "" {
		return "", "", fmt.Errorf("unable to get XSRF token")
	}

	d2lReferrerI := strings.Index(xsrfLine, "d2l_referrer") + 17
	quoteI := strings.Index(xsrfLine[d2lReferrerI:], "\\\"") + d2lReferrerI
	d2lReferrer = xsrfLine[d2lReferrerI:quoteI]

	if activeItemValueLine != "" {
		valueI := strings.Index(activeItemValueLine, "value=") + 7
		quoteI := strings.Index(activeItemValueLine[valueI:], "\"") + valueI
		currentLmsId = activeItemValueLine[valueI:quoteI]
	}

	return
}

func loginToLMS(itemType string) (client *http.Client, err error) {
	client = helpers.HttpClient(true)

	data := url.Values{}
	data.Set("noredirect", "1")
	data.Set("loginPath", "/d2l/login")
	data.Set("userName", os.Getenv("LMS_USER"))
	data.Set("password", os.Getenv("LMS_PASSWORD"))

	_, statusCode := httpRequest(client, "POST", "d2l/lp/auth/login/login.d2l", strings.NewReader(data.Encode()))
	if statusCode >= 400 {
		helpers.Logger().Errorf("unable to log into LMS in order to update a course %s", itemType)
		return nil, fmt.Errorf("failure to loging to lms")
	}

	helpers.Logger().Infof("logged into LMS in order to update a course %s", itemType)

	return client, nil
}

func httpRequest(client *http.Client, method string, url string, body io.Reader) (data io.ReadCloser, statusCode int) {
	req, _ := http.NewRequest(method, os.Getenv("LMS_URL")+url, body)
	if method == "POST" {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	res, err := client.Do(req)
	statusCode = res.StatusCode
	if err != nil || res.StatusCode >= 400 {
		helpers.Logger().Infow("HTTP Request Error",
			"method", method,
			"url", url,
			"statusCode", res.StatusCode,
			"error", err)
		data = nil
		return
	}

	data = res.Body

	return
}
