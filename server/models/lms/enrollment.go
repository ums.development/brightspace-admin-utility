package lms

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
)

type Enrollment struct {
	CourseLmsId string `json:"courseLmsId"`
	UserLmsId   string `json:"userLmsId"`
	Type        string `json:"type"`
}

func PostEnrollment(enrollment Enrollment) error {
	url := "/enrollment"
	payload, err := json.Marshal(enrollment)
	if err != nil {
		return fmt.Errorf("unable to marshal enrollment")
	}

	_, statusCode := helpers.HttpRequest("POST", url, bytes.NewBuffer(payload))
	if statusCode >= 400 {
		return fmt.Errorf("failure adding enrollment to course: %+v", payload)
	}

	return nil
}

func DeleteEnrollment(enrollment Enrollment) error {
	url := "/enrollment"
	payload, err := json.Marshal(enrollment)
	if err != nil {
		return fmt.Errorf("unable to marshal enrollment")
	}

	_, statusCode := helpers.HttpRequest("DELETE", url, bytes.NewBuffer(payload))
	if statusCode >= 400 {
		return fmt.Errorf("failure deleting enrollment to course: %+v", payload)
	}

	return nil
}
