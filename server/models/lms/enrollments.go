package lms

import (
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strconv"
)

type Enrollments struct {
	Identifier   string
	OrgDefinedId string
	FirstName    string
	LastName     string
	RoleId       int
	RoleName     string
}

func GetEnrollmentsByCourse(courseLmsId int) (enrollments []Enrollments, err error) {
	url := fmt.Sprintf("/course/%s/enrollments/", strconv.Itoa(courseLmsId))
	data, statusCode := helpers.HttpRequest("GET", url, nil)
	if statusCode >= 400 {
		enrollments = []Enrollments{}
		err = fmt.Errorf("failure when requesting enrollments info for course %d", courseLmsId)
		return
	}

	err = json.Unmarshal(data, &enrollments)
	if err != nil {
		enrollments = []Enrollments{}
		err = fmt.Errorf("unable to read response for GET %s with data: %s", url, string(data))
		return
	}

	return
}
