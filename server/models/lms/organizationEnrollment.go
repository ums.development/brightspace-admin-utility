package lms

import (
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strconv"
)

type OrganizationEnrollment struct {
	OrgId    int    `json:"org_id"`
	OrgName  string `json:"org_name"`
	OrgCode  string `json:"org_code"`
	RoleId   int    `json:"role_id"`
	RoleName string `json:"role_name"`
}

func GetOrganizationEnrollments(userLmsId int, organizationType string) (enrollments []OrganizationEnrollment, err error) {
	url := fmt.Sprintf("/enrollment/by-organization/%s/%s/", organizationType, strconv.Itoa(userLmsId))
	data, statusCode := helpers.HttpRequest("GET", url, nil)
	if statusCode >= 400 {
		enrollments = []OrganizationEnrollment{}
		err = fmt.Errorf("failure when requesting %s enrollments info for user %d", organizationType, userLmsId)
		return
	}

	err = json.Unmarshal(data, &enrollments)
	if err != nil {
		enrollments = []OrganizationEnrollment{}
		err = fmt.Errorf("unable to read response for GET %s with data: %s", url, string(data))
		return
	}

	return
}
