package lms

import (
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strconv"
)

type Role struct {
	Identifier  string `json:"identifier"`
	DisplayName string `json:"displayName"`
	Code        string `json:"code"`
	ShortCode   string `json:"shortCode"`
}

func GetAllRoles() (roles []Role, err error) {
	helpers.Logger().Debugw("Getting all LMS available Roles",
		"package", "models.lms",
		"method", "GetAllRoles")
	data, statusCode := helpers.HttpRequest("GET", "/role/system/", nil)
	if statusCode >= 400 {
		roles = []Role{}
		err = fmt.Errorf("failure when requesting roles")
		return
	}

	err = json.Unmarshal(data, &roles)
	if err != nil {
		roles = []Role{}
		err = fmt.Errorf("unable to read response for GET /role/system/")
		return
	}

	for i, r := range roles {
		switch r.Identifier {
		case "110":
			roles[i].ShortCode = "S"
			break
		case "109":
			roles[i].ShortCode = "I"
			break
		case "137":
			roles[i].ShortCode = "N"
			break
		case "138":
			roles[i].ShortCode = "X"
			break
		case "133":
			roles[i].ShortCode = "T"
			break
		case "155":
			roles[i].ShortCode = "P"
			break
		case "156":
			roles[i].ShortCode = "Q"
			break
		}
	}

	return
}

func GetRoleNameByInd(roles []Role, roleInd string) string {
	helpers.Logger().Debugw("Get a role name by the indicator",
		"package", "models.lms",
		"method", "GetRoleNameByInd")
	roleInt, err := strconv.Atoi(roleInd)
	if err == nil {
		for _, role := range roles {
			roleId, e := strconv.Atoi(role.Identifier)
			if e == nil && roleInt == roleId {
				return role.DisplayName
			}
		}

		return ""
	}

	for _, role := range roles {
		if roleInd == role.ShortCode {
			return role.DisplayName
		}
	}

	return ""
}

func GetRoleIdByInd(roles []Role, roleInd string) int {
	helpers.Logger().Debugw("Get a role id by the indicator",
		"package", "models.lms",
		"method", "GetRoleIdByInd")
	roleInt, err := strconv.Atoi(roleInd)
	if err == nil {
		return roleInt
	}

	for _, role := range roles {
		if roleInd == role.ShortCode {
			id, err := strconv.Atoi(role.Identifier)
			if err != nil {
				return 0
			}
			return id
		}
	}

	return 0
}
