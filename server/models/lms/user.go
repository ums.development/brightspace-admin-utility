package lms

import (
	"encoding/json"
	"fmt"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"strconv"
)

type User struct {
	FirstName     string
	MiddleName    string
	LastName      string
	UserName      string
	ExternalEmail string
	OrgDefinedId  string
}

func GetUser(userLmsId int) (user User, err error) {
	data, statusCode := helpers.HttpRequest("GET", "/user/"+strconv.Itoa(userLmsId), nil)
	if statusCode >= 400 {
		user = User{}
		err = fmt.Errorf("failure when requesting User info for %d", userLmsId)
		return
	}

	err = json.Unmarshal(data, &user)
	if err != nil {
		user = User{}
		err = fmt.Errorf("unable to read response for GET /user/%d with data: %s", userLmsId, string(data))
		return
	}

	return
}
