package rest

type AcademicUnit struct {
	LmsId         string `json:"lms_id" db:"LMS_ID"`
	TemplateLmsId string `json:"template_lms_id" db:"TEMPLATE_LMS_ID"`
	Term          string `json:"term"`
}
