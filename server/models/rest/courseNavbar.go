package rest

type CourseNavbar struct {
	Institution string `json:"institution" db:"INSTITUTION"`
	Term        string `json:"term" db:"TERM"`
	Navbar      string `json:"navbar"`
}
