package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func AdminPage(router *mux.Router) {
	router.Methods("GET").Path("/all-with-permissions/").HandlerFunc(rest.GetAllActivePagesWithPermissions)
}
