package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func AdminUser(router *mux.Router) {
	router.Methods("GET").Path("/{id:[0-9]+}/").HandlerFunc(rest.AdminUserGetById)
	router.Methods("GET").Path("/all/").HandlerFunc(rest.AdminUserGetAll)
	router.Methods("POST").Path("/").HandlerFunc(rest.AdminUserAdd)
	router.Methods("PUT").Path("/{id:[0-9]+}/").HandlerFunc(rest.AdminUserEdit)
	router.Methods("DELETE").Path("/{id:[0-9]+}/").HandlerFunc(rest.AdminUserDelete)
}
