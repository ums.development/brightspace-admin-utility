package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func AdminUserPermission(router *mux.Router) {
	router.Methods("POST").Path("/").HandlerFunc(rest.AdminUserPermissionAdd)
	router.Methods("DELETE").Path("/{adminUserId:[0-9]+}/{adminPermissionId:[0-9]+}/").HandlerFunc(rest.AdminUserPermissionDelete)
}
