package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func College(router *mux.Router) {
	router.Methods("GET").Path("/set/").HandlerFunc(rest.CollegeGetSet)
	router.Methods("GET").Path("/all/").HandlerFunc(rest.CollegeGetAll)
	router.Methods("PUT").Path("/{id:[0-9]+}/").HandlerFunc(rest.CollegeUpdate)
}
