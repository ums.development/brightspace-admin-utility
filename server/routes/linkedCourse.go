package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func LinkedCourse(router *mux.Router) {
	router.Methods("GET").Path("/children/").HandlerFunc(rest.LinkedCourseChildren)
	router.Methods("DELETE").Path("/{parentCode}/{childCode}/").HandlerFunc(rest.LinkedCourseDelete)
	router.Methods("POST").Path("/").HandlerFunc(rest.LinkedCourseAdd)
}
