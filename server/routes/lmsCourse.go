package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func LmsCourse(router *mux.Router) {
	router.Methods("GET").Path("/{id}").HandlerFunc(rest.LmsCourse)
	router.Methods("POST").Path("/dev/").HandlerFunc(rest.CreateDevCourse)
	router.Methods("POST").Path("/").HandlerFunc(rest.CreateCourse)
	router.Methods("GET").Path("/byCode/").HandlerFunc(rest.LmsCourseByCode)
	router.Methods("PUT").Path("/byCode/").HandlerFunc(rest.PutLmsCourseByCode)
	router.Methods("DELETE").Path("/byCode/").HandlerFunc(rest.DeleteCourse)

}
