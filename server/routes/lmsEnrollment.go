package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func LmsEnrollment(router *mux.Router) {
	router.Methods("POST").Path("/").HandlerFunc(rest.LmsEnrollmentPost)
	router.Methods("DELETE").Path("/").HandlerFunc(rest.LmsEnrollmentDelete)
}
