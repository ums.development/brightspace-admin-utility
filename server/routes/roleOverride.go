package routes

import (
	"github.com/gorilla/mux"
	"gitlab.its.maine.edu/development/lms/utility/server/handlers/rest"
)

func RoleOverride(router *mux.Router) {
	router.Methods("GET").Path("/").HandlerFunc(rest.GetAllRoleOverrides)
	router.Methods("DELETE").Path("/{userLmsId:[0-9]+}/").HandlerFunc(rest.DeleteRole)
	router.Methods("POST").Path("/").HandlerFunc(rest.AddRole)
	router.Methods("GET").Path("/all-roles/").HandlerFunc(rest.GetAllRoles)
}
