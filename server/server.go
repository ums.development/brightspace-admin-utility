package server

import (
	"encoding/gob"
	"github.com/gorilla/sessions"
	"github.com/rs/cors"
	"gitlab.its.maine.edu/development/lms/utility/server/helpers"
	"gitlab.its.maine.edu/development/lms/utility/server/middleware"
	"gitlab.its.maine.edu/development/lms/utility/server/models/db"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var cookieStore = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))

var sessionDataStore = middleware.CreateNewSessionDataStore()

func RunServer() {
	gob.Register(db.AdminUser{})
	gob.Register([]db.AdminPermission{})
	auth, err := middleware.CreateAuth(os.Getenv("AUTH_TYPE"))
	if err != nil {
		helpers.Logger().Fatalw("Unable to create authentication", "error", err)
	}

	err = auth.Init(os.Getenv("AUTH_CONNECTION"))
	if err != nil {
		helpers.Logger().Fatalw("Unable to init authentication", "error", err)
	}

	router := NewRouter(os.Getenv("API_URL"), auth)
	counter := middleware.SyncCounter{}
	router.Use(middleware.RequestId(&counter))
	router.Use(middleware.Log(cookieStore, sessionDataStore))
	router.Use(auth.AuthHandler)
	router.Use(middleware.Context(router, auth, cookieStore, sessionDataStore))
	router.Use(middleware.EnsureAuthorized)

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"http://localhost:3000"},
		AllowedMethods: []string{"GET", "POST", "DELETE", "PUT"},
	})
	handler := c.Handler(router)

	helpers.Logger().Infof("Server startup")

	helpers.Logger().Fatalw("HTTP Server startup failed", "error", http.ListenAndServe(":"+os.Getenv("PORT"), handler))
}

func OnSigKillTerminate() {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-ch
		conn := helpers.DbConn()
		helpers.Logger().Infof("Closing database due to terminate signal")
		err := conn.Close()
		if err != nil {
			helpers.Logger().Infow("Unable to close database", "error", err)
		}
		os.Exit(1)
	}()
}
